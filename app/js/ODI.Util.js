/*
 * OSM Data Integrator
 * 2013-2021 cyrille37@gmail.com
 */
"use strict";

/**
 * Leaflet control displaying ODI stats
 */
ODI.Util = {};

ODI.Util.isCoordValid = function( number )
{
    if( number=='' || number==null || isNaN(number) )
        return false ;
    return true ;
};

ODI.Util.isString = function( o )
{
	return Object.prototype.toString.apply(o) === '[object String]';
}

/**
 * Used to clean input.
 * loadData() will call it for each loaded column.
 */
ODI.Util.cellSanitizeInput = function( str )
{
	var rxIsNum = /^\d+$|^\.\d+$|^\d\.\d*$/;
	if( this.isString(str) )
    {
		// quote numbers that are strings
		if(rxIsNum.test(str)) {
			str = '"' + str + '"';
		} else {
			// escape < and > to avoid XSS
			str = str.replace(/</g, '&lt;');
			str = str.replace(/>/g, '&gt;');
		}
	// convert nulls to '*null*'
	} else if(str === null) {
		str = '*null*';
	}
	return str;
}

ODI.Util.computeBB = function( lat, lng, distance )
{
	// var d = $('#popupInstance .odi_pt_distance').val();
	var p1 = this.moveCoordinatesByMetres( [lat,lng], [-1*distance,-1*distance] );
	var p2 = this.moveCoordinatesByMetres( [lat,lng], [distance,distance] );

	return [ p1, p2 ] ;
}

ODI.Util.moveCoordinatesByMetres = function( latLng, m )
{
	var pi = Math.PI;
	//Earth’s radius, sphere
	var R = 6378137;
	// latLng to transform
	var lat = latLng[0] || latLng.lat;
	var lng = latLng[1] || latLng.lng;

	//Coordinate offsets in radians
	var dLat = m[0] / R;
	var dLng = m[1] / ( R * Math.cos(pi * lat / 180) );

	//OffsetPosition, decimal degrees
	lat = lat + ( dLat * 180 / pi );
	lng = lng + ( dLng * 180 / pi );

	return [lat, lng];
}
