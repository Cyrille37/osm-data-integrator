/*
 * OSM Data Integrator
 * 2013-2014 cyrille37@gmail.com
 */
"use strict";

ODI.OsmDataIntegrator = function ( options )
{
	this.popupTemplateId = options.popupTemplateId ;
	this.mapId = options.mapId ;
	//this.tableId = options.tableId ;
	this.$table = $('#'+options.tableId);

	this.rows ;
	this.columns ;
	this.columnsByName ;
	this.columnIdxLabel = -1 ;
	this.columnIdxLat = -1 ;
	this.columnIdxLon = -1 ;

	this.map ;
	this.layerPoiExt ;
	this.mapDataInvalid = true ;

	this.icons = {
		default: null,
		markerDone: null,
		markerAborted: null,
	};
	this.osmLayer;
	this.layerPoiExt ;
	this.stats ;

	this.searchBB ;

	this.filename ;

	this.localStorage = new ODI.Settings() ;

	// used in event context
	var self = this;

	this.bindGui = function()
	{
		$('#odi-oapiUrl').off('change').on('change', function( ev )
		{
			self.localStorage.oapiUrl.set( $(ev.currentTarget).val() );
		});
		$('#odi-searchQuery').off( 'blur').on( 'blur', function(evt)
		{
			self.localStorage.searchQuery.set( $(evt.currentTarget).val() );
		});
		$('#tab-data-filter-without-coord').off('click').on('click', onBtnFilterWithoutGeoCoord);
		$('#odi_field_label').off('change').on('change', onColumnsMapChange );
		$('#odi_field_lat').off('change').on('change', onColumnsMapChange );
		$('#odi_field_lon').off('change').on('change', onColumnsMapChange );
		$('#odi-filterOn').off('change').on( 'change', function(evt)
		{
			self.localStorage.filter.set( $(evt.currentTarget).val(), null );
			updateFilter( $(evt.currentTarget).val(), null );
			self.mapDataInvalid = true ;
		});
		$('#odi-filterValue').off('change').on( 'change', function(evt)
		{
			self.localStorage.filter.set( undefined, $(evt.currentTarget).val() );
			self.mapDataInvalid = true ;
		});
		$('tbody td .row-id', self.$table).off('click').on('click', onTableRowIdClick );

	};

	this.loadSettings = function()
	{
		var settings = self.localStorage.getData() ;
		console.debug( 'Settings from localStorage:', settings );
		for( var k in settings )
		{
			var s = settings[k];
			if( ! s )
				continue ;
			switch( k )
			{
				case 'searchDistance':
					$('#odi-searchDistance').val( s );
					break
				case 'searchQuery':
					$('#odi-searchQuery').val( s );
					break;
				case 'oapiUrl':
					$('#odi-oapiUrl').val( s );
					break;
				case 'columnsMap':
					if( s.Label )
					{
						this.columnIdxLabel = s.Label ;
						$('#odi_field_label').prop('selectedIndex',this.columnIdxLabel);	
					}
					if( s.Lon )
					{
						this.columnIdxLon = s.Lon ;
						$('#odi_field_lon').prop('selectedIndex',this.columnIdxLon);	
					}
					if( s.Lat )
					{
						this.columnIdxLat = s.Lat ;
						$('#odi_field_lat').prop('selectedIndex',this.columnIdxLat);	
					}
					break;
				case 'filter':
					updateFilter( s.on, s.value );
					break;
				case 'rowsStatus':
					var $tr = $('tbody tr', self.$table);
					for( var row_idx in s )
					{
						switch( s[row_idx] )
						{
							case ODI.Settings.ROW_ABORTED:
								$tr.eq(row_idx).addClass('poi-aborted-bg');
								break;
							case ODI.Settings.ROW_DONE:
								$tr.eq(row_idx).addClass('poi-done-bg');
								break;
							case ODI.Settings.ROW_UNDONE:
								$tr.eq(row_idx).addClass('poi-undone-bg');
								break;
						}
					}
			}
		}
	};

	this.checkRequirements = function()
	{
		// Check for File API support.
		if (window.File && window.FileReader && window.FileList && window.Blob)
		{
		} else {
			alert('The File APIs are not fully supported in this browser.');
		}
	}

	this.loadDataCSV = function( filename, theCSV, datafieldsassocId )
	{
		this.filename = filename ;
		var rows = CSV.csvToArray( theCSV );
		odi.localStorage.load( filename );

		this.loadData( rows, datafieldsassocId );
	}

	var onColumnsMapChange = function(evt)
	{
		var sel = evt.target ;
		var k ;
		switch(evt.target.id)
		{
			case 'odi_field_label' :
				k = 'Label';
				break;
			case 'odi_field_lat' :
				k = 'Lat';
				break;
			case 'odi_field_lon' :
				k = 'Lon';
				break;
			default:
				return ;
		}
		var val = sel.options[sel.selectedIndex].value ;
		self['columnIdx'+k] = val;
		self.mapDataInvalid = true ;
		self.localStorage.columnsMapSet(k, val );
	}

	var updateFilter = function( on, value )
	{
		var $on = $('#odi-filterOn');
		var $value = $('#odi-filterValue');

		// GUI not yet ready
		if( $on.length == 0 )
			return ;

		if( on == null || on == '' )
		{
			$value.html('');
			return ;
		}

		$on.val( on );

		// Create filter values
		var col = self.columnsByName[on],
			out = '',
			label, labels = new Array() ;
		for(var r=1; r < self.rows.length; r++ )
		{
			label = self.rows[r][col];
			if( $.inArray(label, labels) >= 0 )
				continue ;
			labels.push( label );
			out += '<option value="'+label+'">'+label+'</option>' ;
		}
		$value.html( out );
		if( value != null )
			$value.val( value );
	};

	/**
	 * draw rows into a html table
	 */
	this.loadData = function( rows, datafieldsassocId )
	{
		self.mapDataInvalid = true ;

		this.columns = [];
		this.columnsByName = {};
		this.rows = [] ;
		var i, j, row, cell ;

		// Each item in the array is a row
		// walk each row and create table cells for them.

		var out ;

		// Select's options "datafieldsassocId" to affect field to special meanings

		//
		// fields match "name", "lat" & "lon".
		//

		this.columnIdxLat = this.columnIdxLon = this.columnIdxLabel = -1 ;
		var selectOptions = '', selectOptions2 = '';
		row = rows[0];
		for (j = 0; j < row.length; j += 1)
		{
			cell = ODI.Util.cellSanitizeInput( row[j] );
			this.columns[j] = cell ;
			this.columnsByName[cell] = j ;
			selectOptions += '<option value="'+j+'">'+cell+'</option>';
			selectOptions2 += '<option value="'+cell+'">'+cell+'</option>';
			// detect default values
			if( cell == 'lat')
				this.columnIdxLat = j;
			if( cell == 'lon')
				this.columnIdxLon = j;
			if( cell == 'name')
				this.columnIdxLabel = j;
		}
		out = '';
		out += '<dt>label</dt><dd>'
			+'<select id="odi_field_label" name="odi_field_label">' +selectOptions+'</select></dd>'+"\n";
		out += '<dt>lon</dt><dd>'
			+'<select id="odi_field_lon" name="odi_field_lon">' +selectOptions+'</select></dd>'+"\n";
		out += '<dt>lat</dt><dd>'
			+'<select id="odi_field_lat" name="odi_field_lat">' +selectOptions+'</select></dd>'+"\n";

		$('#'+datafieldsassocId).html(out);

		$('#odi_field_label').prop('selectedIndex',this.columnIdxLabel);
		$('#odi_field_lat').prop('selectedIndex',this.columnIdxLat);
		$('#odi_field_lon').prop('selectedIndex',this.columnIdxLon);

		//
		// Filter on a column
		//

		out = '<dt>View only</dt><dd>'
			+'<select id="odi-filterOn">'
			+ '<option value="">No filter</option>'
			+ selectOptions2+'</select></dd>'+"\n";
		out += '<dt>with value</dt><dd>'+'<select id="odi-filterValue"></select></dd>'+"\n";

		$('#'+datafieldsassocId).append(out);

		//
		// Table data
		//

		// table's headers for "tableId"

		out = '';
		out += '<tr>';
		out += '<th>id</th>';
		for (j = 0; j < this.columns.length; j += 1)
		{
			cell = ODI.Util.cellSanitizeInput( this.columns[j] );
			out += '<th>' + cell + '</th>';
		}
		out += '</tr>';
		$('thead',self.$table).html(out);

		// data table's body for "tableId"

		out = '';
		for( i = 1; i < rows.length; i += 1 )
		{
			row = rows[i];
			this.rows[i-1] = row ;
			var row_class = '';
			if( self.columnIdxLat!=null && ! ODI.Util.isCoordValid(row[self.columnIdxLat]) )
				row_class = 'error';
			else if( self.columnIdxLon!= null && ! ODI.Util.isCoordValid(row[self.columnIdxLon]) )
				row_class = 'error';
			out += '<tr class="'+row_class+'">';
			out += '<td><span class="row-id" data-row-id="'+(i-1)+'">' + i + '</span></td>';
			for (j = 0; j < row.length; j += 1)
			{
				cell = ODI.Util.cellSanitizeInput( row[j] );
				out += '<td>' + cell + '</td>';
			}
			out += '</tr>';
		}
		$('tbody', self.$table).html(out);

		// Apply local storage settings
		self.loadSettings();

		// Bind GUI elements to ODI methods
		self.bindGui();
	}

	/**
	 * Toggle hide/show rows with invalid geo coord.
	 */
	var onBtnFilterWithoutGeoCoord = function( ev )
	{
		var $tr = $('tbody tr', self.$table);
		if( ! $(ev.currentTarget).hasClass('active') )
		{
			for( var i in self.rows )
			{
				if( self.rowIsValidPoi(i) )
				{
					$tr.eq(i).hide();
				}
			}	
		}
		else
		{
			$tr.show();
		}
	};

	this.getRowMarker = function( row_idx )
	{
		var marker = null ;
		self.layerPoiExt.eachLayer( function(m)
		{
			if( m.options.poi_row_idx == row_idx )
			{
				//console.debug( 'found marker '+row_idx);
				marker = m ;
				return false ;
			}
		} );
		return marker ;
	}

	/**
	 * Switch to the Map tab, set the view on the marker and display it's popup.
	 * @param {*} e 
	 * @returns 
	 */
	var onTableRowIdClick = function( e )
	{
		if( ! self.layerPoiExt )
		{
			self.mapRefresh();
			// Must invalidate to avoid bad size map ... :-(
			self.mapDataInvalid = true ;
		}

		var row_idx = $(e.currentTarget).data('row-id');
		var marker = self.getRowMarker( row_idx );
		if( marker != null )
		{
			// Switch tab to map
			$('#tabs').tabs( {active: 1} );
			// Set map view on marker
			self.map.setView( marker.getLatLng(), 14 );
			showMarkerPopup( marker );
		}
	};

	this.mapCreate = function ()
	{
		this.map = L.map( this.mapId, {
			contextmenu: true,
			contextmenuItems: [
				{
					text: 'Center map here',
					callback: function( e )
					{
						self.map.panTo(e.latlng);
					}
				}, {
					text: 'Search OSM here',
					callback: function( e )
					{
						var btn = $('.loader', $(this)).addClass('fa-spin');
						self.searchOsm( e.latlng.lat, e.latlng.lng, undefined,
							function()
							{
								btn.removeClass('fa-spin');
							});
					}
				}, {
					text: 'Open in Josm',
					callback: function( e )
					{
						self.openInJosm( e.latlng.lat, e.latlng.lng );
					}
				}
			]
		});

		/**
		 * List of TMS:
		 * - https://josm.openstreetmap.de/wiki/Maps
		 */
		var baseLayers = {
			"OpenStreetMap": new L.TileLayer( 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				subdomains: 'abc',
				minZoom: 1, maxZoom: 19,
				attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
			}),
			"OpenTopoMap": new L.TileLayer( 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
				subdomains: 'abc',
				minZoom: 3, maxZoom: 17,
				attribution: 'Map © data <a href="http://www.openstreetmap.org/copyright">OSM contributors</a>, Tiles <a href="http://www.OpenTopoMap.org">OpenTopoMap</a>'
			}),
			"BDOrtho IGN": new L.TileLayer( 'https://proxy-ign.openstreetmap.fr/94GjiyqD/bdortho/{z}/{x}/{y}.jpg', {
				minZoom: 3, maxZoom: 21,
				attribution: 'Imagery <a href="https://www.openstreetmap.fr/bdortho/">BDOrtho IGN</a>'
			}),
			"Esri World Imagery": new L.TileLayer( 'https://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
				minZoom: 1, maxZoom: 22,
				attribution: 'Map © data <a href="http://www.openstreetmap.org/copyright">OSM contributors</a>, Tiles <a href="http://arcgisonline.com">arcgisonline</a>'
			}),

		};
		L.control.layers(baseLayers).addTo(this.map);
		// Activate a default base layer
		baseLayers["OpenStreetMap"].addTo(this.map);

        var MyCustomAction = LeafletToolbar.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    //html: '&#9873;',
					className: 'fas fa-search-minus',
                    tooltip: 'Go to the Eiffel Tower'
                }
            },
            addHooks: function () {
				//self.map.fitBounds( self.layerPoiExt.getBounds() );
				self.mapFitBounds();
            }

        });
		new LeafletToolbar.Control({
			position: 'topleft',
			actions: [
				MyCustomAction
			]
		}).addTo(this.map);

		this.stats = new ODI.Stats(this.map);

		L.control.mouseCoordinate( {gpsLong:false}).addTo(this.map);

		this.map.addControl( new L.Control.OSMGeocoder() );

		var CustomIcon = L.Icon.extend({
			options: {
				shadowUrl: 'img/marker-shadow.png',
				iconSize:    [25, 41],
				iconAnchor:  [12, 41],
				popupAnchor: [1, -34],
				tooltipAnchor: [16, -28],
				shadowSize: [41, 41]
			}
		});
		this.icons.default = new CustomIcon({
			iconUrl: 'img/marker-icon.png',
		});
		this.icons.markerDone = new CustomIcon({
			iconUrl: 'img/marker-icon-done.png',
		});

		this.icons.markerAborted = new CustomIcon({
			iconUrl: 'img/marker-icon-aborted.png',
		});

	}

	this.mapRefresh = function ()
	{
		console.debug('mapRefresh()', 'mapDataInvalid:', self.mapDataInvalid);

		// Ensure map has the right size #6
		this.map.invalidateSize(true);

		if( ! self.mapDataInvalid )
			return ;

		if( this.layerPoiExt != null)
			this.map.removeLayer(this.layerPoiExt);

		//this.map.invalidateSize(false);

		var markers = this.getMarkers();

		/*if( markers == null )
		{
			self.mapDataInvalid = false ;
			return ;
		}*/

		this.layerPoiExt = L.markerClusterGroup(
		{
			/**
			 * Create the icon for a cluster.
			 * Swith on marker "poi_status".
			 * @param {*} cluster 
			 * @returns void
			 */
			iconCreateFunction: function(cluster)
			{
				// If at least one "undone" ...
				var hasUndone = false ;
				cluster.getAllChildMarkers().every( function(item)
				{
					switch( item.options.poi_status )
					{
						case ODI.Settings.ROW_DONE:
							break;
						case ODI.Settings.ROW_ABORTED:
							break;
						case ODI.Settings.ROW_UNDONE:
						default:
							hasUndone = true ;
							return false ;
					}
					return true ;
				});
				var className = 'marker-cluster';
				if( hasUndone )
				{
					className += ' marker-cluster-large'
				}
				else
				{
					className += ' marker-cluster-small'
				}
				return L.divIcon({
					html: '<div><span>' + cluster.getChildCount() + '</span></div>',
					className: className,
					iconSize: new L.Point(40, 40)
				});
			}
		})
		.addLayer( L.featureGroup(markers) ).addTo(self.map);

		self.mapDataInvalid = false ;
	  
		self.mapFitBounds();
	}

	this.mapFitBounds = function()
	{
		var bounds = self.layerPoiExt.getBounds() ;
		if( bounds._northEast !== undefined )
		{
			self.map.fitBounds( self.layerPoiExt.getBounds() );
		}
		else
		{
			self.map.setView([46.377, 3.439], 6);
		}
	}

	/**
	 * Construct an array of Leaflet markers from data rows.
	 * Apply user filter: if the marker matches the filter it is not added to the array.
	 * 
	 * Each marker will have some "options" (aka data):
	 * - poi_name
	 * - poi_row_idx
	 * - poi_status
	 * @returns Array
	 */
	this.getMarkers = function()
	{
		self.stats.reset( 0 );

		if( ! self.rows )
			return null ;

		var markers = new Array();

		var stats = { undone: 0, done: 0, aborted: 0 };

		var lat, lon, mi = 0, row, lat, lon,
			filter = self.localStorage.filter.get(),
			filterCol = null ;
		if( self.columnsByName[filter.on] !== undefined )
			filterCol = self.columnsByName[filter.on];

		for( var i=0; i< self.rows.length; i++)
		{
			row = this.rows[i];

			// filter on a column

			if( filterCol != null )
			{
				// transform to String in case of filter.value == "null".
				if( (''+row[filterCol]) != filter.value )
				{
					continue ;
				}
			}

			// Create marker

			lat = 1*this.rows[i][this.columnIdxLat] ;
			lon = 1*this.rows[i][this.columnIdxLon] ;

			var icon = this.icons.default ;
			var poi_status = ODI.Settings.ROW_UNDONE;
			if( this.rowIsDone( i ) )
			{
				poi_status = ODI.Settings.ROW_DONE;
				icon = this.icons.markerDone ;
				stats.done ++ ;
			}
			else if( this.rowIsAborted( i ) )
			{
				poi_status = ODI.Settings.ROW_ABORTED;
				icon = this.icons.markerAborted ;
				stats.aborted ++ ;
			}
			else
			{
				stats.undone ++ ;
			}

			if( lat!='' && lon!='' && lat!=0 && lon!=0)
			{
				markers[mi++] = L.marker([lat,lon],{
					icon: icon,
					poi_name: row[this.columnIdxLabel],
					poi_row_idx: i,
					poi_status: poi_status
				})
				.on('click', function(e)
				{
					showMarkerPopup( e.target );
				});
			}
		}

		self.stats.reset( self.rows.length );
		self.stats.set(ODI.Stats.UNDONE, stats.undone );
		self.stats.set(ODI.Stats.DONE, stats.done );
		self.stats.set(ODI.Stats.ABORTED, stats.aborted );

		return markers ;
	}

	var showMarkerPopup = function( m )
	{
		var
			pp = $('#'+self.popupTemplateId).clone(),
			mLat = m.getLatLng().lat,
			mLng = m.getLatLng().lng ;

		// Construct marker's popup with POI's data row

		$('.odi_pt_title',pp).html(m.options.poi_name);

		$('.odi_pt_distance',pp).attr('value', self.localStorage.searchDistance.get() );

		$('.odi_pt_row',pp).html('');
		var row = self.rows[m.options.poi_row_idx];
		for( var i=0; i<self.columns.length; i++)
		{
			$('.odi_pt_row',pp).append('<dt>'+self.columns[i]+'</dt><dd>'+(row[i]!=''?row[i]:'&nbsp;')+'</dd>');
		}

		// searchDistance
		pp.on( "change", ".odi_pt_distance", function(event)
		{
			self.localStorage.searchDistance.set( $('.odi_pt_distance', pp).val() );
		});

		pp.on( "click",".odi_search_btn",
			{
				lat: mLat,
				lng: mLng,
				row_idx: m.options.poi_row_idx
			},
			function(event)
			{
				var btn = $('.loader', $(this)).addClass('fa-spin');
		
				self.searchOsm( event.data.lat, event.data.lng, event.data.row_idx,
					function()
					{
						btn.removeClass('fa-spin');
					});
			}
		);

		pp.on( 'click', '.odi_josm_btn',
			{ lat: mLat, lng: mLng },
			function( event ) {
				self.openInJosm( event.data.lat, event.data.lng );
			}
		);
		// FIXME: odi.openInJosm(). should not know "odi" variable name
		//$('.odi_josm_btn',pp).attr('onclick',
		//	'odi.openInJosm('+mLat+','+mLng+','+'"popupInstance"'+')');

		pp.on( 'click', '.odi_done_btn',
			{ poi_row_idx: m.options.poi_row_idx },
			function( event ) {
				//log( event.data.poi_row_idx);
				self.rowSetDone( event.data.poi_row_idx );
				m.setIcon( self.icons.markerDone );
				m.options.poi_status = ODI.Settings.ROW_DONE;
				self.layerPoiExt.refreshClusters(m);
			}
		);

		pp.on( 'click', '.odi_aborted_btn',
			{ poi_row_idx: m.options.poi_row_idx },
			function( event ) {
				//log( event.data.poi_row_idx);
				self.rowSetAborted( event.data.poi_row_idx );
				m.setIcon( self.icons.markerAborted );
				m.options.poi_status = ODI.Settings.ROW_ABORTED;
				self.layerPoiExt.refreshClusters(m);
			}
		);

		pp.on( 'click', '.odi_todo_btn',
			{ poi_row_idx: m.options.poi_row_idx },
			function( event ) {
				//log( event.data.poi_row_idx);
				self.rowSetUndone( event.data.poi_row_idx );
				m.setIcon( self.icons.default );
				m.options.poi_status = ODI.Settings.ROW_UNDONE;
				self.layerPoiExt.refreshClusters(m);
			}
		);

		pp.show();
		L.popup( {offset: [0,-20]} )
			.setLatLng(m.getLatLng())
			.setContent(pp[0])
			.openOn(self.map)
			;

	};

	/**
	 * 
	 * @param {string} searchQuery 
	 * @param {integer} row_idx 
	 * @returns 
	 */
	this.queryReplaceVariables = function(searchQuery, row_idx)
	{
		if( row_idx === undefined )
			return searchQuery ;

		var variablesRe = /{{([A-Za-z0-9_]+)}}/;
		return searchQuery.replace(variablesRe, function(chn, p1, decalage, s)
		{
			if( self.columnsByName[p1] === undefined )
				return ;
			var v = self.rows[row_idx][self.columnsByName[p1]];
			return v ;
		});
	}

	/**
	 * @param lat Double center of search
	 * @param lng Double center of search
	 * @param distance Double distance from center on search
	 * @param searchQuery String Overpass QL with optionals {{variables}}
	 */
	this.searchOsm = function( lat,lng, row_idx, callback )
	{
		var searchQuery = self.localStorage.searchQuery.get() ;

		if( searchQuery==null || searchQuery.trim() == '' )
		{
			alert('Missing a search query');
			return ;
		}

		if( self.osmLayer != null )
		{
			self.map.removeLayer(self.osmLayer);
			self.osmLayer=null;
		}

		// remove previous rectangle
		if( self.searchBB != null )
			self.map.removeLayer(self.searchBB);

		// create an orange rectangle
		var bounds = ODI.Util.computeBB(lat,lng,self.localStorage.searchDistance.get());
		self.searchBB = L.rectangle(bounds, {
			color: "#ff7800",
			weight: 1
		}).addTo( self.map);
		bounds = self.searchBB.getBounds();

		// http://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide#Find_something
		// https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL
		// Château de [vV]almer

		searchQuery = this.queryReplaceVariables( searchQuery, row_idx );

		// Global bounding box: [bbox:south,west,north,east]
		// southern-most latitude, western-most longitude, northern-most latitude, eastern-most longitude
		var bbox = ''+bounds.getSouthEast().lat+','+bounds.getNorthWest().lng+','+bounds.getNorthWest().lat+','+bounds.getSouthEast().lng+'';

		var q='[bbox:'+bbox+']' ;
		q+= '[out:json]';
		q+= '[timeout:60];';
		// gather results
		q+= searchQuery;
		// print results
		q+= 'out ; >> ; out ;';

		var q = encodeURIComponent(q);

		var url = self.localStorage.oapiUrl.get() + '?data=' + q  ;

		var jqxhr = $.ajax(
		{
			url: url,
			type: 'GET',
			dataType: 'json'
		})
		.done(function(data, textStatus, jqXHR)
		{
			self.map.fitBounds(bounds);

			if( data.elements.length == 0 )
			{
				alert('noting found.');
				return ;
			}

			// Display a dialog with result data

			var w = $('#multiResultDialog') ;
			$('table tbody tr', w).remove();
			$(data.elements).each(function()
			{
				var tr = '<tr><td>'+this.type+'</td><td><a href="https://osm.org/'+this.type+'/'+this.id+'" target="_blank">'+this.id+'</a></td><td></td></tr>';
				for (const k in this.tags) {
					tr += '<tr><td></td><td>'+k+'</td><td>'+this.tags[k]+'</td></tr>';
				}
				$('table tbody', w).append($(tr)) ;
			});
			w.dialog();

			// Render results on the map

			self.renderResults( data );

		})
		.fail(function(jqXHR, textStatus, errorThrown)
		{
			alert('ajax fail'+"\n"+textStatus+"\n"+errorThrown);
		})
		.always(function(data_jqXHR, textStatus, jqXHR_errorThrown)
		{
			if( callback !== undefined )
				callback();
		});
	},

	this.renderResults = function ( data )
	{
		var data_mode = 'json' ;
		self.osmLayer = new L.OSM4Leaflet(null, {
			data_mode: data_mode,
			baseLayerClass: L.GeoJsonNoVanish,
			//baseLayerClass: L.GeoJSON,
			baseLayerOptions: {
				threshold: 9*Math.sqrt(2)*2,
				compress: function(feature) {
					return !(feature.properties.mp_outline && $.isEmptyObject(feature.properties.tags));
				},
				style: function(feature) {
					var stl = {};
					var color = "#03f";
					var fillColor = "#fc0";
					var relColor = "#d0f";
					// point features
					if (feature.geometry.type == "Point") {
						stl.color = color;
						stl.weight = 2;
						stl.opacity = 0.7;
						stl.fillColor = fillColor;
						stl.fillOpacity = 0.3;
					}
					// line features
					else if (feature.geometry.type == "LineString") {
						stl.color = color;
						stl.opacity = 0.6;
						stl.weight = 5;
					}
					// polygon features
					else if ($.inArray(feature.geometry.type, ["Polygon","MultiPolygon"]) != -1) {
						stl.color = color;
						stl.opacity = 0.7;
						stl.weight = 2;
						stl.fillColor = fillColor;
						stl.fillOpacity = 0.3;
					}

					// style modifications
					// tainted objects
					if (feature.properties && feature.properties.tainted==true) {
						stl.dashArray = "5,8";
					}
					// multipolygon outlines without tags
					if (feature.properties && feature.properties.mp_outline==true)
						if (typeof feature.properties.tags == "undefined" ||
							$.isEmptyObject(feature.properties.tags)) {
							stl.opacity = 0.7;
							stl.weight = 2;
						}
					// objects in relations
					if (feature.properties && feature.properties.relations && feature.properties.relations.length>0) {
						stl.color = relColor;
					}

					if (feature.is_placeholder) {
						stl.fillColor = "red";
					}

					return stl;
				},
				pointToLayer: function (feature, latlng) {
					return new L.CircleMarker(latlng, {
						radius: 9
					});
				},
				onEachFeature : function (feature, layer) {
					layer.on('click', function(e) {
						var popup = "";
						if (feature.properties.type == "node")
							popup += "<h3>Node <a href='http://www.openstreetmap.org/browse/node/"+feature.properties.id+"' target='_blank'>"+feature.properties.id+"</a></h3>";
						else if (feature.properties.type == "way")
							popup += "<h3>Way <a href='http://www.openstreetmap.org/browse/way/"+feature.properties.id+"' target='_blank'>"+feature.properties.id+"</a></h3>";
						else if (feature.properties.type == "relation")
							popup += "<h3>Relation <a href='http://www.openstreetmap.org/browse/relation/"+feature.properties.id+"' target='_blank'>"+feature.properties.id+"</a></h3>";
						else
							popup += "<h3>"+feature.properties.type+" #"+feature.properties.id+"</h2>";
						if (feature.properties && feature.properties.tags && !$.isEmptyObject(feature.properties.tags)) {
							popup += '<h4>Tags:</h4><ul class="plain">';
							$.each(feature.properties.tags, function(k,v) {
								k = htmlentities(k); // escaping strings!
								v = htmlentities(v);
								popup += "<li>"+k+"="+v+"</li>"
							});
							popup += "</ul>";
						}
						if (feature.properties && feature.properties.relations && !$.isEmptyObject(feature.properties.relations)) {
							popup += '<h3>Relations:</h3><ul class="plain">';
							$.each(feature.properties.relations, function (k,v) {
								popup += "<li><a href='http://www.openstreetmap.org/browse/relation/"+v["rel"]+"' target='_blank'>"+v["rel"]+"</a>";
								if (v.reltags &&
									(v.reltags.name || v.reltags.ref || v.reltags.type))
									popup += " <i>" +
									$.trim((v.reltags.type ? htmlentities(v.reltags.type)+" " : "") +
										(v.reltags.ref ?  htmlentities(v.reltags.ref)+" " : "") +
										(v.reltags.name ? htmlentities(v.reltags.name)+" " : "")) +
									"</i>";
								if (v["role"])
									popup += " as <i>"+htmlentities(v["role"])+"</i>";
								popup += "</li>";
							});
							popup += "</ul>";
						}
						if (feature.properties && feature.properties.meta && !$.isEmptyObject(feature.properties.meta)) {
							popup += '<h3>Meta:</h3><ul class="plain">';
							$.each(feature.properties.meta, function (k,v) {
								k = htmlentities(k);
								v = htmlentities(v);
								popup += "<li>"+k+"="+v+"</li>";
							});
							popup += "</ul>";
						}
						if (feature.geometry.type == "Point")
							popup += "<h3>Coordinates:</h3><p>"+feature.geometry.coordinates[1]+" / "+feature.geometry.coordinates[0]+" <small>(lat/lon)</small></p>";
						if ($.inArray(feature.geometry.type, ["LineString","Polygon","MultiPolygon"]) != -1) {
							if (feature.properties && feature.properties.tainted==true) {
								popup += "<p><strong>Attention: incomplete geometry (e.g. some nodes missing)</strong></p>";
							}
						}
						var p = L.popup({},this).setLatLng(e.latlng).setContent(popup);
						p.layer = layer;
						//fire("onPopupReady", p);
						p.openOn(self.map);
					});
				}
			}
		});
		self.osmLayer.addData(data);

		self.map.addLayer(self.osmLayer);

	}

	/**
	 * Call JOSM remoting
	 * http://wiki.openstreetmap.org/wiki/JOSM/Plugins/RemoteControl#List_of_Commands
	 */
	this.openInJosm = function(lat,lng)
	{
		var b = ODI.Util.computeBB(lat,lng, self.localStorage.searchDistance.get() );
		var url =  'http://127.0.0.1:8111/load_and_zoom?top='+b[1][0]+'&left='+b[0][1]+'&bottom='+b[0][0]+'&right='+b[1][1] ;
		//log(url);

		$.ajax(
		{
			url: url,
			type: 'GET',
			dataType: 'text'
		})
		.done(function(data, textStatus, jqXHR) {
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.error('ajax fail! textStatus:', textStatus, 'error:', errorThrown);
		});
	};

	this.rowSetStatus = function( row_idx, status )
	{
		var $tr = $('tbody tr', self.$table).eq(row_idx);
		// previous status
		switch( self.localStorage.rowStatus.get(row_idx) )
		{
			case ODI.Settings.ROW_UNDONE:
				self.stats.dec(ODI.Stats.UNDONE );
				$tr.removeClass('poi-undone-bg');
				break;
			case ODI.Settings.ROW_DONE:
				self.stats.dec(ODI.Stats.DONE );
				$tr.removeClass('poi-done-bg');
				break;
			case ODI.Settings.ROW_ABORTED:
				self.stats.dec(ODI.Stats.ABORTED );
				$tr.removeClass('poi-aborted-bg');
				break;
		}
		// new status
		switch( status )
		{
			case ODI.Settings.ROW_UNDONE:
				self.stats.inc(ODI.Stats.UNDONE );
				$tr.addClass('poi-undone-bg');
				break;
			case ODI.Settings.ROW_DONE:
				self.stats.inc(ODI.Stats.DONE );
				$tr.addClass('poi-done-bg');
				break;
			case ODI.Settings.ROW_ABORTED:
				self.stats.inc(ODI.Stats.ABORTED );
				$tr.addClass('poi-aborted-bg');
				break;
		}
		// update localStorage
		self.localStorage.rowStatus.set( row_idx, status );
	};

	this.rowSetDone = function( row_idx )
	{
		self.rowSetStatus( row_idx, ODI.Settings.ROW_DONE);
	};
	this.rowIsDone = function( row_idx )
	{
		return self.localStorage.rowStatus.get(row_idx) == ODI.Settings.ROW_DONE ;
	}

	this.rowSetAborted = function( row_idx )
	{
		self.rowSetStatus( row_idx, ODI.Settings.ROW_ABORTED);
	},
	this.rowIsAborted = function( row_idx )
	{
		return self.localStorage.rowStatus.get(row_idx) == ODI.Settings.ROW_ABORTED ;
	},

	this.rowSetUndone = function( row_idx )
	{
		self.rowSetStatus( row_idx, ODI.Settings.ROW_UNDONE);
	},
	this.rowIsUndone = function( row_idx )
	{
		return self.localStorage.rowStatus.get(row_idx) == ODI.Settings.ROW_UNDONE ;
	};

	this.rowIsValidPoi = function( row_idx )
	{
		var lat = self.rows[row_idx][self.columnIdxLat],
			lon = self.rows[row_idx][self.columnIdxLon];
		if( lat=='' || lat==null || isNaN(lat) )
		{
		}
		else if( lon=='' || lon==null || isNaN(lon) )
		{
		}
		else
		{
			return true ;
		}
		return false ;
	};

	this.mapCreate();

}
