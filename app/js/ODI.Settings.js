/*
 * OSM Data Integrator
 * 2013-2014 cyrille37@gmail.com
 */
"use strict";

/**
 * 
 */
ODI.Settings = function ()
{
	ODI.Settings.NO_FILE = 'none';
	ODI.Settings.ROW_UNDONE = 0 ;
	ODI.Settings.ROW_DONE = 1 ;
	ODI.Settings.ROW_ABORTED = -1 ;
	
	var data = {
		filename: null,
		searchQuery: null,
		searchDistance: 500,
		rowsStatus: {},
		columnsMap: {},
		filter: {
			on: null,
			value: null
		},
		oapiUrl: null
	};

	var self = this ;

	this.load = function( filename )
	{
		data.filename = filename ;
		if( ! hasLocalStorage )
			return null ;
		var json = localStorage.getItem( data.filename );
		if( json == null )
			return null ;
		data = JSON.parse(json);
	};
	this.load( ODI.Settings.NO_FILE );

	function localStorageAvailable()
	{
		if(typeof localStorage=='undefined')
			return false ;
		return true ;
	};
	var hasLocalStorage = localStorageAvailable() ;

	function updateLocalStorage()
	{
		if( ! hasLocalStorage )
			return ;
		localStorage.setItem( data.filename, JSON.stringify(data));	
	};

	this.getData = function()
	{
		return data ;
	}

	this.rowSetDone = function ( row_idx )
	{
		data.rowsStatus[row_idx] = ODI.Settings.ROW_DONE ;
		updateLocalStorage() ;
	};

	this.rowIsDone = function( row_idx )
	{
		if( data.rowsStatus[row_idx] !== undefined
			&& data.rowsStatus[row_idx] == ODI.Settings.ROW_DONE )
			return true  ;
		return false ;
	};

	this.rowSetAborted = function ( row_idx )
	{
		data.rowsStatus[row_idx] = ODI.Settings.ROW_ABORTED ;
		updateLocalStorage() ;
	};

	this.rowIsAborted = function( row_idx )
	{
		if( data.rowsStatus[row_idx] !== undefined
			&& data.rowsStatus[row_idx] == ODI.Settings.ROW_ABORTED )
			return true  ;
		return false ;
	};

	this.rowSetDefault = function ( row_idx )
	{
		delete data.rowsStatus[row_idx];
		updateLocalStorage() ;
	};

	this.rowStatus = {
		get: function( row_idx ){ return data.rowsStatus[row_idx] },
		set: function( row_idx, status ){
			switch( status )
			{
				case ODI.Settings.ROW_ABORTED:
				case ODI.Settings.ROW_DONE:
					data.rowsStatus[row_idx] = status ;
					break;
				case ODI.Settings.ROW_UNDONE:
					delete data.rowsStatus[row_idx] ;
					break;
				default:
					alert('Invalid row status "'+status+'" for row "'+row_idx+'"');
					return ;
			}
			updateLocalStorage() ;
		}
	};

	this.columnsMapGet = function( field )
	{
		if( data.columnsMap[field] === undefined )
			return null ;
		return data.columnsMap[field];
	}

	this.columnsMapSet = function( field, value )
	{
		data.columnsMap[field] = value ;
		updateLocalStorage() ;
	}

	this.filter = {
		get: function(){ return data.filter },
		set: function( on, value )
		{
			if( on !== undefined )
				data.filter.on = on ;
			data.filter.value = value ;
			updateLocalStorage() ;
		}
	};

	this.searchQuery = {
		get: function(){ return data.searchQuery },
		set: function( searchQuery ){ data.searchQuery = searchQuery ; updateLocalStorage() ; }
	};

	this.searchDistance = {
		get: function(){ return data.searchDistance },
		set: function( searchDistance ){ data.searchDistance = searchDistance ; updateLocalStorage() ; }
	};

	this.oapiUrl = {
		get: function(){ return data.oapiUrl },
		set: function( oapiUrl ){ data.oapiUrl = oapiUrl ; updateLocalStorage() ; }
	};

	this.forget = function()
	{
		if( ! hasLocalStorage )
			return ;
		if( data.filename == null )
			return ;
		localStorage.removeItem( data.filename );			
	}
}
