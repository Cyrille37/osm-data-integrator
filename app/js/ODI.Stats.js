/*
 * OSM Data Integrator
 * 2013-2021 cyrille37@gmail.com
 */
"use strict";

/**
 * Leaflet control displaying ODI stats
 */
ODI.Stats = function ( map )
{
    ODI.Stats.TOTAL = 'total';
    ODI.Stats.UNDONE = 'undone';
    ODI.Stats.DONE = 'done';
    ODI.Stats.ABORTED = 'aborted';

    this.data = {
      total: 0, undone: 0, done: 0, aborted: 0
    };

    var self = this ;

    var html_id = 'map_stats' ;
    var $ihm, ihm ;

    L.control.custom({
        position: 'topright',
        content: '<div id="'+html_id+'">'
            +'<p>Stats</p><dl>'
            +'<dt class="">Points</dt><dd class="total"></dd>'
            +'<dt class="poi-done">Done</dt><dd class="done"></dd>'
            +'<dt class="poi-aborted">Aborted</dt><dd class="aborted"></dd>'
            +'<dt class="poi-undone">Undone</dt><dd class="undone"></dd>'
            +'</dl></div>'
    })
    .addTo(map);

    $ihm = $('#'+html_id);
    ihm = {
        $total: $('dd.total',$ihm),
        $undone: $('dd.undone',$ihm),
        $done: $('dd.done',$ihm),
        $aborted: $('dd.aborted',$ihm)
    };

    this.refresh = function()
    {
        ihm.$total.html(self.data.total);
        ihm.$undone.html(self.data.undone);
        ihm.$done.html(self.data.done);
        ihm.$aborted.html(self.data.aborted);
    }

    this.reset = function( total )
    {
        self.data.total = total ;
        self.data.undone = 0;
        self.data.done = 0;
        self.data.aborted = 0;
        self.refresh();
    }

    this.set = function( key, value )
    {
        self.data[key] = value ;
        self.refresh();
    }
    this.inc = function( key )
    {
        self.data[key] ++ ;
        self.refresh();
    }
    this.dec = function( key )
    {
        self.data[key] -- ;
        self.refresh();
    }
}
