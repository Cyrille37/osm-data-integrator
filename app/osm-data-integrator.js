/*
 * OSM Data Integrator
 * 2013-2014 cyrille37@gmail.com
 */
"use strict";

var ODI = {};
var odi ;

//
// Load dependencies
//

var scripts = [
	{file: 'app/js/ODI.Util.js' },
	{file: 'app/js/ODI.Settings.js' },
	{file: 'app/js/ODI.Stats.js' },
	{file: 'app/js/ODI.OsmDataIntegrator.js', onloaded: odi_init }	
];
for( var i in scripts )
{
	var script  = document.createElement('script');
	script.src  = scripts[i].file;
	script.type = 'text/javascript';
	//script.defer = false;
	if( scripts[i].onloaded !== undefined )
		script.onload = scripts[i].onloaded;
	document.getElementsByTagName('head').item(0).appendChild(script);  
}

function odi_init()
{
	odi = new ODI.OsmDataIntegrator( {
		popupTemplateId: 'popupTemplate',
		mapId: 'map',
		tableId: 'tab-data-grid'
	});
	odi.checkRequirements();
	odi.loadSettings();
	odi.bindGui();

	var hash = new L.Hash( odi.map );

};

//
//
//

function log(msg)
{
	window.console && console.log(msg);
}

