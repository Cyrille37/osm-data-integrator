# OSM Data Integrator

Pour faciliter l'intégration de données dans OpenStreetMap. Charger un fichier CSV avec au moins 3 colonnes (name, latitude, longitude) pour visualiser les données en tableau et sur une carte. Sur la carte vous pouvez marquer les "points" comme "faits", "abandonnés" ou "à faire". Rechercher via overpass autour des points, ouvrir ces environs dans Josm. Les réglages sont mémorisés pour chaque fichier dans le navigateur web.

Code source & discussions sur [framagit.org/Cyrille37/osm-data-integrator](https://framagit.org/Cyrille37/osm-data-integrator).

## Mode d'emploi

1. charger un fichier de données au format CSV (séparées par des virgules)
    - soit 3 colonnes sont déjà nommées "name", "lat" et "lon"
    - sinon faire la correspondance avec les listes déroulantes
3. aller sur l'onglet "carte" pour voir les données sur une carte.
4. cliquer sur un marqueur

## Installation

Tout se passe dans le navigateur, aucun serveur n'est nécessaire : on peut ouvrir l'application en mode "file://".

Licence: GPL v3 - http://www.gnu.org/licenses/quick-guide-gplv3.fr.html

## Merci à ...

- [jquery-1.9](api.jquery.com/)
- [jquery-ui](https://jqueryui.com/) v1.10
- [bootstrap](https://getbootstrap.com/) v2
- [leaflet](https://leafletjs.com) 0.7
- [fontawesome](https://fontawesome.com) Free v5
- [leaflet-hash](https://github.com/mlevans/leaflet-hash)
- [Leaflet.Control](https://github.com/yigityuce/Leaflet.Control.Custom)
- [Leaflet.contextmenu](https://github.com/aratcliffe/Leaflet.contextmenu)
- [leaflet.MarkerCluster](https://github.com/Leaflet/Leaflet.markercluster)
- [Leaflet.toolbar](https://github.com/Leaflet/Leaflet.toolbar)
- [leaflet.mouseCoordinate](https://github.com/PowerPan/leaflet.mouseCoordinate)
- [leaflet-control-osm-geocoder](https://github.com/k4r573n/leaflet-control-osm-geocoder)
- [ucsv](https://github.com/uselesscode/ucsv) A Useless Code javascript CSV livrary
- [overpass-turbo](https://github.com/tyrasd/overpass-turbo/)

